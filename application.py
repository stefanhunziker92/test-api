from datetime import datetime
import random
import string
import jwt
import boto3
from werkzeug.exceptions import Unauthorized
import requests
import json
import flask
from flask import Flask, request, Response, make_response, jsonify
from flask import send_from_directory
from flask import send_file
from flask_cors import CORS
from flask_restplus import Api, Resource, inputs, fields, marshal
import werkzeug
from base64 import b64encode
werkzeug.cached_property = werkzeug.utils.cached_property


# config
application = flask.Flask(__name__)
application.config["PROPAGATE_EXCEPTIONS"] = False
application.config.SWAGGER_UI_DOC_EXPANSION = 'full'
api = Api(application, version='1.0', title='Elastic-Beanstalk')

# parser for diez-send-message
parser_eb = api.parser()

parser_eb.add_argument('token', type=str, required=False,
                                      help='Bearer Access Token.')

parser_eb.add_argument('attachment', type=werkzeug.datastructures.FileStorage, location='files',
                    required=False, help='Files to be attached to the email')



# models
token = fields.String(example='"eyJraWQiOiI5M2NHeEY5dHhINTl2SWJpVDR1NDZQQ0VvOFNGZ2xEdno5THg1UVRcL2k3Yz0iLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJjOWRmNTUzMC0zOGQ3LTQzMTEtODM5Zi00OThmNTMzZDU0ZmYiLCJjb2duaXRvOmdyb3VwcyI6WyJiYXNpY3BsYW4iXSwiZXZlbnRfaWQiOiI5MzA2Yjk3NC1hZWE3LTQ4MGYtYjY4Yi1kZDhjZTkzMzhmZmMiLCJ0b2tlbl91c2UiOiJhY2Nlc3MiLCJzY29wZSI6ImF3cy5jb2duaXRvLnNpZ25pbi51c2VyLmFkbWluIiwiYXV0aF90aW1lIjoxNjAyMDY1NDEzLCJpc3MiOiJodHRwczpcL1wvY29nbml0by1pZHAuZXUtY2VudHJhbC0xLmFtYXpvbmF3cy5jb21cL2V1LWNlbnRyYWwtMV9uZTYwczlEUXkiLCJleHAiOjE2MDIwNjkwMTMsImlhdCI6MTYwMjA2NTQxMywianRpIjoiN2QxYjI4YmYtYmIxNS00MzcyLWFiYjktN2ExZmRmMDUzOWM2IiwiY2xpZW50X2lkIjoiM3JzanRidHNmMmJ2NWYxODdkOTk4OWQyc2wiLCJ1c2VybmFtZSI6ImM5ZGY1NTMwLTM4ZDctNDMxMS04MzlmLTQ5OGY1MzNkNTRmZiJ9.RS-tvI0-Fdb8gvJAxvr2jVSs3ChhPAlXEGGkJu2EoA28PgVdKFfqui9IRzHtGVZi4CDpRs8ZAsA7g7FApF5bJmONnWeu2WWpghMsuEZFixiaEfVlCajhukMZZZC_KiAyxCdKGwti5_DbvWpmGBwdqdlQOWOLhZFPCm2XgnxFp_3ANrqZEJhXwGDTekxl-AMyb1L1s-msE6-5EgVqv9ZggTg9eEwNFc_VfKw87ahDP8sK9Qgz6ZPTsozhCo9vXMQ-YfAlAAicIiK-nJoSwo3fhjLMF-rgcFPySDS9ePWKb08vJfyWLp3-itZHwmn6d5ULKJkE0Q8e-5SSUNfUIX1dxQ"')

# login für diez


@api.route('/eb', doc={"description": "try new model"})
class Logindiez(Resource):
    @api.response(400, 'Validation Error')
    @api.response(401, 'Unauthorized Error')
    @api.response(200, 'Success')
    @api.expect(parser_eb)
    def post(self):
        args = parser_eb.parse_args()

        # don't check auth on local host
        # req_from_localhost = bool(re.match("http:\/\/127.0.0.1.*\/", request.url_root)
        #                       ) or bool(re.match("http:\/\/localhost.*\/", request.url_root))
        # if not req_from_localhost:
        #   token = args["token"]
        #    username = get_user_name(token)

        # Log into the chat
        rslt = try_eb(args)

        return rslt


def try_eb(args):
    filename = args['attachment'].filename
    return filename
    


# get user
def get_user_name(token):
    region = "eu-central-1"
    provider_client = boto3.client("cognito-idp", region_name=region)
    try:
        user = provider_client.get_user(AccessToken=token)
    except provider_client.exceptions.NotAuthorizedException as e:
        raise Unauthorized('Invalid token!')
    return user["Username"]


@ api.errorhandler(ValueError)
def handle_value_exception(error):
    err_message = str(error)
    return {'message': err_message}, 400


@api.representation('image/jpeg')
def img_response(data, code, headers=None):
    response = application.make_response(data)
    response.headers['Content-Type'] = 'image/jpeg'
    return resp


if __name__ == "__main__":
    application.run()
